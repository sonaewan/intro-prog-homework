# exercise 1
def odd_or_even(number):
    if number%2 == 0:
        print('this is even')
    else:
        print('this is odd')
        

# exercise 2
list_with_prices = [11, 20, 15, 8, 4]

def products_promotion(prices):
    for_promotion = []
    not_for_promotion = []
    for price in list_with_prices:
        if price > 5 and price < 10: 
            for_promotion.append(price)
        else:
            not_for_promotion.append(price)
    return len(for_promotion)

number_promotion_products = products_promotion(list_with_prices)


# exercise 3
def get_dictionary(n):
    r = range(1, n)
    return {k:k*k for k in r}


# exercise 4, 5, 6 and 7 
# you should make the necessary tweaks in the function according to the exercise
    
cars = [
  {'brand': 'BMW', 'registration_year': '2008', 'price': 17800},
  {'brand': 'AUDI', 'registration_year': '2012', 'price': 25000},
  {'brand': 'Mercedes-Benz', 'registration_year': '2019', 'price': 80000}
  ]


def get_car(brand=False, reg_year=False, price=False):
    render_cars = []
    for car in cars:
        if car['brand'] == brand:
            render_cars.append(car)
    return render_cars[0]

display_car = get_car(brand='BMW')
