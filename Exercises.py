def even_odd(num):
    if num%2==0:
        print("Even")
    else: print("odd")
    

def shop(pricelist):
    x=0
    for product in pricelist:
        if pricelist[product]>=5 and pricelist[product]<10:
            x=x+1
    if x>=5 :
        print("The number of products is", x, ", too many products")
    else: print("The number of products is", x)

def square_func(n):
    dicti={}
    for i in range(1,n+1,1):
        dicti.update({i:i*i})
    print (dicti)
    
list_of_cars = [
  {'brand': 'AUDI', 'model': 'Q2 Dsl', 'past_owners': ['Miguel', 'Francisca'], 'registration_year': '2017', 'price': 17800}, 
  {'brand': 'AUDI', 'model': 'A6 SW Dsl', 'past_owners': ['Antonio'], 'registration_year': '2013', 'price': 52000},
  {'brand': 'SKODA', 'model': 'Fabia', 'past_owners': ['Josh'], 'registration_year': '2012', 'price': 11000},
  {'brand': 'BMW', 'model': '520', 'past_owners': ['Mike', 'Angela', 'Rodrigo'], 'registration_year': '2014', 'price': 62000},
  {'brand': 'AUDI', 'model': 'A4 SW Dsl', 'past_owners': ['Francesco'], 'registration_year': '2016', 'price': 93000},
  {'brand': 'VOLKSWAGEN', 'model': 'Touran', 'past_owners': ['Luke', 'Phill'], 'registration_year': '2017', 'price': 87000},
  {'brand': 'AUDI', 'model': 'A4 Dsl', 'past_owners': ['Christian', 'Sam', 'James'], 'registration_year': '2010', 'price': 62000},
  {'brand': 'BMW', 'model': '535', 'past_owners': ['Sheldon', 'Leonard'], 'registration_year': '2014', 'price': 68000},
  {'brand': 'BMw', 'model': '330', 'past_owners': ['Carlsen'], 'registration_year': '2018', 'price': 75000},
  {'brand': 'BmW', 'model': '320', 'past_owners': ['Muhammad'], 'registration_year': '2018', 'price': 75000}]

def car_select(client_need):
    potential_cars=[]
    for car in list_of_cars:
        if car["brand"].upper()==client_need["brand"].upper() or client_need["brand"]=="":
            if car["model"]==client_need["model"] or car["model"][0]==client_need["model"] or client_need["model"]=="":
                if len(car["past_owners"])<=client_need["past_owners"][1] and len(car["past_owners"])>=client_need["past_owners"][3]:
                    if int(car["registration_year"])>=client_need["registration_year"][0] and int(car["registration_year"])<=client_need["registration_year"][1]:
                        if car["price"]<=client_need["price"]["stop"] and car["price"]>=client_need["price"]["start"]:
                            potential_cars.append(car)
    if potential_cars==[]:
        print("No car is available")
    else: print("the available cars are", potential_cars)
    
def car_select_2(brand="",model="",past_owners=[100,0],registration_year=(1900,2019),price=(0,1000000)):
    potential_cars=[]
    for car in list_of_cars:
        if car["brand"].upper()==brand or brand=="":
            if car["model"]==model or car["model"][0]==model or model=="":
                if len(car["past_owners"])<=past_owners[0] and len(car["past_owners"])>=past_owners[1]:
                    if int(car["registration_year"])>=registration_year[0] and int(car["registration_year"])<=registration_year[1]:
                        if car["price"]<=price[1] and car["price"]>=price[0]:
                            potential_cars.append(car)
    if potential_cars==[]:
        print("No car is available")
    else: print("the available cars are", potential_cars)